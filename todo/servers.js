//'use strict';
const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');
const http = require('http');

// const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger/swagger.json');

const bodyParser = require('body-parser');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
  extended: true
}));

// app.use(express.static(path.join(__dirname, 'dist/express-kcs')));

// app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument, { explorer: true }));

app.use(cors({
  'allowedHeaders': ['sessionId', 'Content-Type', 'x-access-token', 'mimeType'],
  'exposedHeaders': ['sessionId', 'x-access-token'],
  'origin': '*',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false,
  'credentials': false
}));

// app.use('/v1/api', require('./routes/index'));

// app.use((req, res) => {
//   res.sendFile(__dirname + '/dist/express-kcs/index.html')
// })

// var server = http.createServer(app);
// var port = process.env.PORT || 9702;

// server.listen(port, function () {
//   console.log(new Date(), __filename, `Server active on ${port}`);
// });

// module.exports = app;

let port = 4064;

app.listen(port, function () {
    console.log('Node app is running on port', port);
});
 
module.exports = app;
